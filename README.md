# Goal Prediction (Polynomial regression / Logisitic regression)

### By: Andrew Wairegi

## Description
To create a prediction model using the FIFA dataset, of international matches. 
This will allow me to to create a model, that the Mchezopesa ltd can use to predict wins/loses (Logistic regression) in their tournament/league. 
As well as goal count predictions (Polynomial regression). Note, this dataset has data on draws, so it will allow me to incorporate 
draws in to the model aswell. 

[Open notebook]

## Setup/installation instructions
1. Find a local folder on your computer
2. Setup your folder as an empty repository (using git init)
3. Clone the remote repository here, into your local folder (using git clone https://...)
4. Upload the collaboratory notebook to google drive
5. Open it online
6. Upload the data files in your repository to the google collab, in the file upload section
7. Run the notebook

## Known Bugs
There are no known problems/issues

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package
7. Scipy - A Statistics package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/Polynomial_Logisitic-Goal_Prediction_Fifa/-/blob/main/Fifa_Goal_Prediction_(polynomial_regression_logisitic_regression).ipynb
